# mardowntest

# Initial Setup

Lets see if lists are displayed properly.

Some text just to make a filler to make sure there is something to print on the page. But I really
think this is quite a bit of text to show. maybe now I should stop.

1. This is list item one
   
   **[+ NOTE: +]** Testing an indented line
   
   ```
   testing indented code box
   ```

2. This sholud be the second item

   ```
   another indented code box
   ```

3. A list item for number 3

4. Fourth list item

   ```
   Final indented code box
   ```

That's it. Did the list display properly?
